using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] [Range(0.1f, 7f)] float smoothSpeed = .125f;
    Vector3 offset;
    Vector3 smooothVelocity = Vector3.zero;

    private void Start()
    {
        offset = transform.position - playerTransform.position;
    }

    private void LateUpdate()
    {
        if (playerTransform != null)
        {
            Vector3 desiredPos = playerTransform.position + offset;
            transform.position = Vector3.SmoothDamp(transform.position, desiredPos, ref smooothVelocity, smoothSpeed * Time.deltaTime);
            //transform.LookAt(playerTransform);
        }
    }
}
