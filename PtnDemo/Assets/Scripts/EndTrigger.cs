using System.Collections;
using UnityEngine;

public class EndTrigger : MonoBehaviour
{
    public event System.Action onLevelComplete;
    [SerializeField] private Transform paintingWall;
    [SerializeField] private Transform fallenPos;
    [SerializeField] private float fallingTime = 2;
    bool wallHasFallen;


    // when the player reaches around the end circle painting wall will falling to paint it
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!wallHasFallen)
            {
                if (onLevelComplete != null)
                {
                    onLevelComplete();
                }

                paintingWall.gameObject.SetActive(true);
                StartCoroutine(paintingWallFalling());
            }
        }
    }

    IEnumerator paintingWallFalling() {
        float percent = 0;
        float fallingSpeed = 1 / fallingTime;
        Vector3 initialPos = paintingWall.position;        
        while (percent <= 1)
        {
            percent += Time.deltaTime * fallingSpeed;
            paintingWall.position = Vector3.Lerp(initialPos, fallenPos.position, percent);
            yield return null;
        }
        wallHasFallen = true;
    }
}
