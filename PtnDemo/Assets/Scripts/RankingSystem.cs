using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class RankingSystem : MonoBehaviour
{
    public Runner[] runners;
    int numberofRunners;
    bool gameHasEnded;
    private void Start()
    {
        FindObjectOfType<EndTrigger>().onLevelComplete += gameEnded;
        runners = new Runner[11];
        Invoke("getAllRunners", .2f);
    }    

    private void getAllRunners()
    {
        numberofRunners = runners.Length;
        runners = FindObjectsOfType<Runner>();

        //for (int i = 0; i < numberofRunners; i++)
        //{
        //    print(runners[i].name);               //writing from the last so = en9---en1---player
        //}

        StartCoroutine(sortRunners());
    }

    IEnumerator sortRunners()
    {
        while (!gameHasEnded)
        {
            float refreshRate = .3f;
            runners = runners.OrderBy(x => x.zAxisDistanceFromSpawnPoint).ToArray();    // its sorting by Z axis
            for (int i = 1; i <= numberofRunners; i++)
            {
                runners[i - 1].rank = numberofRunners + 1 - i;      // runners[0] is the last one so his ranks should be 11
                //print(runners[i-1].name +" "+runners[i - 1].rank);
            }                                                       // runners[10] is the firs one so his ranks should be 1;
            yield return new WaitForSeconds(refreshRate);
        }
    }

    void gameEnded()
    {
        gameHasEnded = true;
    }
}
