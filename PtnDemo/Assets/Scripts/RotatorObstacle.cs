using System;
using UnityEngine;

public class RotatorObstacle : MonoBehaviour
{
    [SerializeField] private float rotateSpeed = -90;
    [SerializeField] private float forceAmount = 150;

    private void Update()
    {
        rotate();
    }

    private void rotate()
    {
        transform.Rotate(Vector3.up * rotateSpeed * Time.deltaTime);
    }

    private void OnCollisionStay(Collision collision)
    {
        collision.gameObject.GetComponent<Rigidbody>().AddForce((-collision.transform.forward * forceAmount * Time.deltaTime),
        ForceMode.VelocityChange);
    }
}
