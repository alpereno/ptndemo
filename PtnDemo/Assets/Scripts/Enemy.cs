using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class Enemy : Runner
{
    enum State { idle, run };
    public string enemyName { get; private set; }
    [SerializeField] Color currentColor;
    [SerializeField] private Transform[] targetTransforms;
    
    Animator anim;
    NavMeshAgent pathFinder;
    State currentState;

    int targetTransformIndex;
    int targetLength;
    bool reachedToFinish;    

    void Awake()
    {
        pathFinder = GetComponent<NavMeshAgent>();
    }
    private void Start()
    {                                                                          // when the player reaches the end point stop running
        FindObjectOfType<EndTrigger>().onLevelComplete += stopRunning;         // event subscription
        targetTransformIndex = 0;
        targetLength = targetTransforms.Length;
        StartCoroutine(updatePath());
        currentState = State.run;
        anim = GetComponent<Animator>();
    }

    protected override void Update()
    {
        if (!reachedToFinish)
        {
            base.Update();
            if (currentState == State.run)
            {
                animateRunning(true);
            }

            if (targetTransformIndex >= targetLength)
            {
                stopRunning();
                reachedToFinish = true;
            }
            else
            {   // checks if the target to be reached is close
                // We don't need to actual distance
                // I did squaredDistance on the left side of the inequality
                if (sqrDistanceXZ(pathFinder.transform.position, targetTransforms[targetTransformIndex].position) < 6.5f)
                {
                    targetTransformIndex++;
                }
            }
        }
    }

    private void animateRunning(bool isRunning)
    {
        anim.SetBool("EIsRunning", isRunning);
    }

    IEnumerator updatePath() {
        float refreshRate = .2f;
        while (!reachedToFinish)
        {
            if (targetTransformIndex < targetLength)
            {
                pathFinder.SetDestination(targetTransforms[targetTransformIndex].position);
            }            
            yield return new WaitForSeconds(refreshRate);
        }
    }

    void stopRunning() {
        currentState = State.idle;
        StopCoroutine(updatePath());
        pathFinder.velocity = Vector3.zero;
        pathFinder.ResetPath();
        reachedToFinish = true;
        animateRunning(false);
    }

    // To create differences between enemies when Spawner spawn all enemies
    public void setCharacteristicProp(string _enemyName,Vector3 spawnPos, float speed, Color newColor, int priority, float acceleration = 25) {
        enemyName = _enemyName;
        transform.name = enemyName;
        spawnPoint = spawnPos;

        pathFinder.speed = speed;
        pathFinder.acceleration = acceleration;
        pathFinder.avoidancePriority = priority;

        currentColor = newColor;
        GetComponentInChildren<Renderer>().material.color = currentColor;
    }

    // I've used to Square  not SquareRoot because square root operation is fairly expensive
    // we don't need the actual distance
    float sqrDistanceXZ(Vector3 from, Vector3 to)
    {
        float distance = (from.x - to.x) * (from.x - to.x)
            + (from.z - to.z) * (from.z - to.z);

        return distance;
    }
}