using UnityEngine;

public class PaintGunController : MonoBehaviour
{
    [SerializeField] private Transform weaponHold;                  // the gun was not in our hands from the beginning
    [SerializeField] private PaintGun gunToEquip;                   // when reaches the end point player will equip
    PaintGun equippedPaintGun;

    private void Start()
    {
        GameObject.FindObjectOfType<EndTrigger>().onLevelComplete += equipPaintGun;
    }

    void equipPaintGun() {
        if (equippedPaintGun != null)
        {
            Destroy(equippedPaintGun.gameObject);
        }
        equippedPaintGun = Instantiate(gunToEquip, weaponHold.position, weaponHold.rotation) as PaintGun;
        equippedPaintGun.transform.parent = weaponHold;
    }

    public void shoot() {
        if (equippedPaintGun != null)
        {
            equippedPaintGun.shoot();
        }
    }

    public void lookAt(Vector3 aimPoint)
    {
        if (equippedPaintGun != null)
        {
            equippedPaintGun.lookAt(aimPoint);
        }
    }
}
