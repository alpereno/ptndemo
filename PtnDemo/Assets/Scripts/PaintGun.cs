using System.Collections;
using UnityEngine;

public class PaintGun : MonoBehaviour
{
    // PaintGun to shoot if the player reaches the end
    [SerializeField] private Transform muzzle;
    [SerializeField] private GameObject paintBallEffect;
    [SerializeField] private LayerMask paintLayer;
    [SerializeField] private float msBetweenShots;

    float nextShotTime;

    public void shoot()
    {
        if (Time.time > nextShotTime)
        {
            nextShotTime = Time.time + msBetweenShots / 1000;
            Ray ray = new Ray(muzzle.position, muzzle.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100, paintLayer))
            {
                Vector3 dir = (hit.point - transform.position).normalized;
                GameObject paintObject = Instantiate(paintBallEffect, hit.point - dir * .1f, Quaternion.Euler(-90, 0, 0));
            }
        }
    }

    public void lookAt(Vector3 aimPoint)
    {
        //Vector3 correctPoint = new Vector3(aimPoint.x, transform.position.y, aimPoint.z);
        //transform.LookAt(correctPoint);
        transform.LookAt(aimPoint);
    }
}