using System;
using UnityEngine;

[RequireComponent (typeof (Rigidbody))]
public class Player : Runner
{
    [SerializeField] private float speed = 5;
    [SerializeField] private Animator anim;
    [SerializeField] private LayerMask targetMask;
    //[SerializeField] private GameObject crossHair;

    PaintGunController gunController;
    Camera viewCamera;
    Rigidbody playerRb;
    Vector3 moveVelocity;
    float horizontalInput, verticalInput;
    bool playerCanMoveForward;

    private void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        FindObjectOfType<EndTrigger>().onLevelComplete += onLevelCompleted;
        gunController = GetComponent<PaintGunController>();
        spawnPoint = transform.position;
        playerCanMoveForward = true;
        viewCamera = Camera.main;
        //crossHair.SetActive(false);
    }

    protected override void Update()
    {
        base.Update();
        moveInput();
        checkPlayerYPosition();
        weaponInput();
        createAimRay();
    }

    private void FixedUpdate()
    {
        move();
    }

    private void move()
    {
        playerRb.MovePosition(playerRb.position + moveVelocity * Time.fixedDeltaTime);
    }

    private void moveInput()
    {
        if (playerCanMoveForward)
        {
            verticalInput = Input.GetAxisRaw("Vertical");            
        }

        horizontalInput = Input.GetAxisRaw("Horizontal");
        moveVelocity = new Vector3(horizontalInput, 0, verticalInput).normalized;
        moveVelocity *= speed;

        animateLRRunning(verticalInput, horizontalInput);
        animateRunning(verticalInput);
    }

    void weaponInput()
    {
        if (Input.GetMouseButton(0))
        {
            gunController.shoot();
        }
    }

    void createAimRay() {
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        float distance = 50;
        if (Physics.Raycast(ray, out hit, distance, targetMask, QueryTriggerInteraction.Ignore))
        {
            gunController.lookAt(hit.point);
            Vector3 lookPoint = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            transform.LookAt(lookPoint);
            //crossHair.transform.position = hit.point - Vector3.forward * .1f;
            Debug.DrawLine(ray.origin, hit.point, Color.red);
        }
    }

    private void animateRunning(float verticalInput)
    {
        bool running = verticalInput != 0f;
        anim.SetBool("IsRunning", running);
    }

    private void animateLRRunning(float verticalInput, float horizontalInput) {
        bool justLRRunning = verticalInput == 0 && horizontalInput != 0;
        anim.SetBool("IsLRRunning", justLRRunning);
    }

    private void checkPlayerYPosition()
    {
        if (transform.position.y < -10)
        {
            respawn();
        }
    }

    void onLevelCompleted() {
        playerCanMoveForward = false;
        verticalInput = 0;
        moveVelocity = Vector3.zero;
        //crossHair.SetActive(true);
    }
}
