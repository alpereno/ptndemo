using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private GameObject mainMenuHolder;
    [SerializeField] private GameObject optionsMenuHolder;
    [SerializeField] private Toggle[] resolutionToggles;
    [SerializeField] private Toggle fullScreenToggle;
    [SerializeField] private int[] screenWidths;

    [Header("Effects")]
    [SerializeField] private Image fadeImage;
    int activeScreenResIndex;

    private void Start()
    {
        activeScreenResIndex = PlayerPrefs.GetInt("screen res index", activeScreenResIndex);
        bool isFullscreen = (PlayerPrefs.GetInt("fullscreen") == 1) ? true : false;

        for (int i = 0; i < resolutionToggles.Length; i++)
        {
            resolutionToggles[i].isOn = i == activeScreenResIndex;
        }
        fullScreenToggle.isOn = isFullscreen;
    }


    public void play()
    {
        StartCoroutine(fadeAndStartGame());
        //SceneManager.LoadScene("Game");
    }

    public void quit()
    {
        Application.Quit();
    }

    public void optionsMenu()
    {
        mainMenuHolder.SetActive(false);
        optionsMenuHolder.SetActive(true);
    }

    public void mainMenu()
    {
        mainMenuHolder.SetActive(true);
        optionsMenuHolder.SetActive(false);
    }

    public void setScreenResolution(int i)
    {
        if (resolutionToggles[i].isOn)
        {
            activeScreenResIndex = i;
            float aspectRatio = 3 / 5f;
            Screen.SetResolution(screenWidths[i], (int)(screenWidths[i] / aspectRatio), false);
            PlayerPrefs.SetInt("screen res index", activeScreenResIndex);
            PlayerPrefs.Save();
        }
    }

    public void setFullScreen(bool isFullScreen)
    {
        for (int i = 0; i < resolutionToggles.Length; i++)
        {
            resolutionToggles[i].interactable = !isFullScreen;
        }
        if (isFullScreen)
        {
            Resolution[] allResolutions = Screen.resolutions;
            Resolution maxResolutions = allResolutions[allResolutions.Length - 1];
            Screen.SetResolution(maxResolutions.width, maxResolutions.height, true);
        }
        else
        {
            setScreenResolution(activeScreenResIndex);
        }
        PlayerPrefs.SetInt("fullscreen", (isFullScreen) ? 1 : 0);
        PlayerPrefs.Save();
    }

    IEnumerator fadeAndStartGame() {
        fadeImage.gameObject.SetActive(true);
        float time = 1.3f;
        float speed = 1 / time;
        float percent = 0;
        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            fadeImage.color = Color.Lerp(Color.clear, new Color(0, 0, 0, 0.95f), percent);
            yield return null;
        }
        SceneManager.LoadScene("Game");

    }
}
