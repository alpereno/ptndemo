using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Enemy enemy;               // this class spawns all enemies in a certain area
    [SerializeField] private int enemyNumber;           // and assigns different properties to all of them.
    [SerializeField] private Vector2 enemyspeedMinMax;
    [SerializeField] private Vector2 enemyObstaclePriorityMinMax;
    [SerializeField] private Color[] enemyColor;

    Collider areaCollider;
    Vector2 spawnAreaXMinMax;
    Vector2 spawnAreaZMinMax;

    private void Start()
    {
        areaCollider = GetComponent<BoxCollider>();
        spawnAreaXMinMax = new Vector2(areaCollider.bounds.min.x, areaCollider.bounds.max.x);
        spawnAreaZMinMax = new Vector2(areaCollider.bounds.min.z, areaCollider.bounds.max.z);

        spawnAllEnemies(enemyNumber);
    }

    private void spawnAllEnemies(int enemyNumber)
    {
        for (int i = 0; i < enemyNumber; i++)
        {
            Vector3 randomEnemyPos = new Vector3(Random.Range(spawnAreaXMinMax.x, spawnAreaXMinMax.y), transform.position.y,
                Random.Range(spawnAreaZMinMax.x, spawnAreaZMinMax.y));
            float enemySpeed = Random.Range(enemyspeedMinMax.x, enemyspeedMinMax.y);
            int priority = (int)Random.Range(enemyObstaclePriorityMinMax.x, enemyObstaclePriorityMinMax.y);

            Enemy spawnedEnemy = Instantiate(enemy, randomEnemyPos, Quaternion.identity) as Enemy;
            string spawnedEnemyName= "Enemy " + i;
            spawnedEnemy.setCharacteristicProp(spawnedEnemyName, randomEnemyPos, enemySpeed, enemyColor[i], priority, 25);
        }
    }
}
