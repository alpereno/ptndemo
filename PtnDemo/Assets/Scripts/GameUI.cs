using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
    [SerializeField] RankingSystem rankSystem;
    [SerializeField] RectTransform paintWallBanner;
    [SerializeField] GameObject gameOverUI;
    [SerializeField] Image fadeImage;

    //[SerializeField] GameObject percentUI;
    //[SerializeField] TMP_Text percentText;
    [SerializeField] GameObject finishPaintingButton;

    [SerializeField] GameObject rankList;
    [SerializeField] TMP_Text[] rankListName;

    [SerializeField] Canvas canvas;
    float screenQuarterHeight;
    float screenHeight;

    private void Start()
    {
        FindObjectOfType<EndTrigger>().onLevelComplete += onLevelComplete;
        screenHeight = (canvas.GetComponent<RectTransform>().rect.height);
        screenQuarterHeight = screenHeight / 4f;
    }

    void onLevelComplete() {
        StartCoroutine(animateBanner());
        rankList.SetActive(false);
        finishPaintingButton.SetActive(true);
        //percentUI.gameObject.SetActive(true);
    }

    // finish painting button
    public void gameOver() {
        StartCoroutine(fade(Color.clear, new Color(0, 0, 0, .95f), 2));
        finishPaintingButton.SetActive(false);
        gameOverUI.SetActive(true);
    }

    private void Update()
    {
        if (rankList.gameObject.activeInHierarchy)
        {
            for (int i = 0; i <= 10; i++)
            {
                if (rankSystem.runners[i] != null)
                {
                    rankListName[i].text = rankSystem.runners[10-i].name;
                }                
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            startNewGame();
        }
    }

    IEnumerator fade(Color from, Color to, float time)
    {
        float speed = 1 / time;
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            fadeImage.color = Color.Lerp(from, to, percent);
            yield return null;
        }
    }

    IEnumerator animateBanner()
    {
        //Banner will go down firstly then go back to up
        float delayTime = 2.5f;
        float speed = 3f;
        float animatePercent = 0;
        float endDelayTime = Time.time + 1 / speed + delayTime;
        int direction = -1;

        while (animatePercent >= 0)
        {
            animatePercent -= Time.deltaTime * speed * direction;

            if (animatePercent >= 1)
            {
                animatePercent = 1;
                if (Time.time > endDelayTime)
                {
                    direction = 1;
                }
            }
            paintWallBanner.anchoredPosition = Vector2.up * Mathf.Lerp(screenHeight - 1.45f * screenQuarterHeight,
                screenHeight - 2.8f * screenQuarterHeight, animatePercent);
            yield return null;
        }
    }

    // Play/PlayAgain button
    public void startNewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Menu button
    public void returnToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
