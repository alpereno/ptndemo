using System.Collections;
using UnityEngine;

public class Runner : MonoBehaviour
{
    //public event System.Action onDeath;
                    // base class of enemy and player. This class created for Reference in the ranking list etc..
    public float zAxisDistanceFromSpawnPoint { get; protected set; }
    public int rank;
    protected Vector3 spawnPoint;

    protected virtual void Update()
    {
        calculateDistance();
    }

    // to rank list
    public void calculateDistance()
    {
        zAxisDistanceFromSpawnPoint = transform.position.z;
        //zAxisDistanceFromSpawnPoint = transform.position.z - spawnPoint.z;      // it's just fairer
    }                                                                             // cause it's not the same all of participants of z axis
                                                                                  // but the above is easier to observe
    public void respawn() {
        transform.position = spawnPoint;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Obstacle"))
        {
            respawn();
        }
    }
}
