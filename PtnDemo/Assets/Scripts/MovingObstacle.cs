using System.Collections;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    [SerializeField] private Transform pathHolder;      // stores the path Obstacle will goes on. u can chance whatever u want
    [SerializeField] private float speed;               // pathHolder occurs of empty GameObjects
    [SerializeField] private float waitTime = .3f;      // It doesn't matter how many just gotta be child object of pathholder.
                                                        // Obstacle will follow them.
    [SerializeField] private Transform[] rayPoss;
    [SerializeField] private LayerMask targetMask;
    [SerializeField] bool isDonut;                      // HalfDonut obstacle and Horizontal obstacle are do almost the same thing
                                                        // go and return. But one of them doesn't make the runner fail
                                                        // So to avoid code duplications I've included this bool
    private void Start()
    {
        Vector3[] wayPoints = new Vector3[pathHolder.childCount];
        for (int i = 0; i < wayPoints.Length; i++)
        {
            wayPoints[i] = pathHolder.GetChild(i).position;
        }

        StartCoroutine(followPath(wayPoints));
    }

    private void Update()
    {
        if (!isDonut)
        {
            for (int i = 0; i < rayPoss.Length; i++)
            {
                Ray ray = new Ray(rayPoss[i].position, rayPoss[i].up);
                //Debug.DrawLine(rayPoss[i].position, rayPoss[i].position + rayPoss[i].up * .5f, Color.green);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, .6f, targetMask))
                {
                    Runner runner = hit.collider.GetComponent<Runner>();
                    runner.respawn();
                }
            }
        }

    }


    IEnumerator followPath(Vector3[] wayPoints) {
        transform.position = wayPoints[0];
        int targetWayPointIndex = 1;
        Vector3 targetWayPoint = wayPoints[targetWayPointIndex];

        while (true)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetWayPoint, speed * Time.deltaTime);
            if (transform.position == targetWayPoint)
            {
                targetWayPointIndex = (targetWayPointIndex + 1) % wayPoints.Length;
                targetWayPoint = wayPoints[targetWayPointIndex];
                yield return new WaitForSeconds(waitTime);
            }
            yield return null;
        }
    }
}
